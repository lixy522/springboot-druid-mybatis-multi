package com.heibaiying.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/*  程序运行:
* ① localhost:8080/db/programmers
* ② locahost:8080/druid/login.html
* ③ 注意用户名和密码都为 druid
* ④ 即可在web页面中查看到mysql链接信息。
*/


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class DruidMybatisMultiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DruidMybatisMultiApplication.class, args);
    }

}

